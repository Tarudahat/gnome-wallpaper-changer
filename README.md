# Gnome Wallpaper Changer

This little script changes your Gnome Desktop wallpaper based on the images in the provided directory.

## Configuration
The source directory and time between images can be changed. Find the desktop entry in `~/.config/autostart` and add the appropriate flag:

```
--path [default: ~/Pictures]    // Select the directory on your filesystem
--sleep [default: 600]    // The time to wait before changing to a new image (seconds)
```

So your desktop entry might look like:

```
[Desktop Entry]
Name=gnome-wallpaper-changer
Exec=/usr/bin/gnome-wallpaper-changer --path /home/<user_name>/Pictures/Wallpapers --sleep 300
Comment=Automatically change wallpaper
Hidden=false
Type=Application
X-GNOME-Autostart-enabled=true
```
Note: the path to your wallpaper folder must be an absolute one.

## Installation
### Arch
This repo is mainly created to serve as src for an AUR package.

Run, with an AUR-enabled package manager;
```
$ <package_manager> -Syu gnome-wallpaper-changer-git
```

### Manual
Copy the `gnome-wallpaper-changer` to a place somewhere on your system and create a desktop entry in `~/.config/autostart`.
